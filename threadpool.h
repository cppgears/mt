#pragma once
#include <thread>
#include <vector>
#include <list>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <queue>
#include <functional>

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>

namespace gear {
	namespace mt {
		using namespace std;
		
		class threadpool {
		public:
			class job {
			public:
				virtual ~job() { ; }
				inline std::thread::id thread_id() { return std::this_thread::get_id(); }
				inline void sleep(size_t microsecond) { std::this_thread::sleep_for(std::chrono::microseconds(microsecond)); }
			protected:
				friend class threadpool;
				virtual void operator()() = 0;
			};
		private:
			list<thread>			pool_threads;
			queue<shared_ptr<job>>	pool_jobs;
			size_t					thread_index;
			atomic_bool				pool_yet;
			mutex					pool_jobs_sync;
			condition_variable		pool_condition;
			inline void expand(size_t total) {
				for (; total-- ; ) {
					pool_threads.emplace_back(thread([](size_t core, atomic_bool& yet, mutex& sync, condition_variable& wait, queue<shared_ptr<job>>& jobs) {
						auto timeout = std::chrono::milliseconds(1000);
						do {
							shared_ptr<job> task;
							{
								std::unique_lock<std::mutex> lock(sync);
								wait.wait_for(lock, timeout, [&jobs, &yet] {return !yet || !jobs.empty(); });
								if (!yet) break;
								if (jobs.empty()) continue;
								task = jobs.front();
								jobs.pop();
							}
							(*task)();
						} while (yet);
					}, thread_index++, ref(pool_yet), ref(pool_jobs_sync), ref(pool_condition), ref(pool_jobs)));
				}
			}
			inline void vacuum() {
			}
		public:
			threadpool(size_t initial_count, vector<size_t> cores = {}) {
				thread_index = 0;
				pool_yet = true;
				expand(initial_count);
			}

			inline size_t count() { return pool_threads.size(); }
			inline size_t length() { return pool_jobs.size(); }
			template<typename T>
			inline void push(T* task) {
				if (pool_yet) {
					{
						std::unique_lock<std::mutex> lock(pool_jobs_sync);
						pool_jobs.emplace((job*)task);
						/*if (pool_jobs.size() >= pool_threads.size()) {
							expand(pool_threads.size() + thread_grow_by);
						}*/
					}
					pool_condition.notify_one();
				}
				else {
					delete task;
				}
			}

			inline void join() {
				pool_yet = false;
				for (auto&& th : pool_threads) {
					th.join();
				}
				pool_threads.clear();
				/*
				pool_jobs.clear([](job*& el) {
					delete el;
				});*/
			}
		};
	}
}