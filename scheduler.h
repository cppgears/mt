#pragma once
#include <list>
#include <thread>

namespace gear {
	namespace mt {
		using namespace std;
		class scheduler {
		public:
			class job {
			public:
				virtual bool operator()() = 0;
				virtual ~job() { ; }
			};

			scheduler() : _is_running(false) { ; }
			~scheduler() {
				stop();
			}
			template<typename JOB>
			scheduler& task(JOB* task, size_t period_seconds, bool execute_now = false) {
				tasks.emplace_back(0, period_seconds, (job*)task);
				if (execute_now) {
					std::get<0>(tasks.back()) = time(nullptr) + period_seconds;
					if (!(*(std::get<2>(tasks.back())))()) {
						delete std::get<2>(tasks.back());
						tasks.pop_back();
					}
				}
				return *this;
			}
			inline void foreground() {
				for (auto&& task : tasks) {
					if (time(nullptr) >= std::get<0>(task)) {
						std::get<0>(task) = time(nullptr) + std::get<1>(task);
						if (!(*(std::get<2>(task)))()) {
							delete std::get<2>(task);
							tasks.pop_back();
						}
					}
				}
			}
			inline void background(size_t timeout_millisecond = 500) {
				if (!_background.joinable()) {
					_is_running = true;
					_background = thread([&](volatile bool& running, size_t millisecond) {
						while (running) {
							this_thread::sleep_for(std::chrono::milliseconds(millisecond));
							foreground();
						}
					},ref(_is_running), timeout_millisecond ? timeout_millisecond : 500);
				}
			}
			inline void stop() {
				_is_running = false;
				if (_background.joinable()) {
					_background.join();
				}
				for (auto&& task : tasks) {
					delete std::get<2>(task);
				};
			}
		private:
			std::list<std::tuple<time_t /*last execution time*/, size_t /* period seconds */, job*>> tasks;
			thread		_background;
			volatile	bool _is_running;
		};
	}
}